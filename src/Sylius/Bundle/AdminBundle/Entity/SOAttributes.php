<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * SOAttributes
 */
class SOAttributes implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $sONumber;

    /**
     * @var string
     */
    private $productCode;

    /**
     * @var string
     */
    private $defaultName;

    /**
     * @var string
     */
    private $defaultValue;

    /**
     * @var string
     */
    private $defaultString;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sONumber
     *
     * @param integer $sONumber
     *
     * @return SOAttributes
     */
    public function setSONumber($sONumber)
    {
        $this->sONumber = $sONumber;

        return $this;
    }

    /**
     * Get sONumber
     *
     * @return int
     */
    public function getSONumber()
    {
        return $this->sONumber;
    }

    /**
     * Set productCode
     *
     * @param string $productCode
     *
     * @return SOAttributes
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;

        return $this;
    }

    /**
     * Get productCode
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * Set defaultName
     *
     * @param string $defaultName
     *
     * @return SOAttributes
     */
    public function setDefaultName($defaultName)
    {
        $this->defaultName = $defaultName;

        return $this;
    }

    /**
     * Get defaultName
     *
     * @return string
     */
    public function getDefaultName()
    {
        return $this->defaultName;
    }

    /**
     * Set defaultValue
     *
     * @param string $defaultValue
     *
     * @return SOAttributes
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Set defaultString
     *
     * @param string $defaultString
     *
     * @return SOAttributes
     */
    public function setDefaultString($defaultString)
    {
        $this->defaultString = $defaultString;

        return $this;
    }

    /**
     * Get defaultString
     *
     * @return string
     */
    public function getDefaultString()
    {
        return $this->defaultString;
    }
}

