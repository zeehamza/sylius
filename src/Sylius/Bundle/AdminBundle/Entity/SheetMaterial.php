<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * SheetMaterial
 */
class SheetMaterial implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $materialCode;

    /**
     * @var int
     */
    private $gSM;

    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $width;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set materialCode
     *
     * @param string $materialCode
     *
     * @return SheetMaterial
     */
    public function setMaterialCode($materialCode)
    {
        $this->materialCode = $materialCode;

        return $this;
    }

    /**
     * Get materialCode
     *
     * @return string
     */
    public function getMaterialCode()
    {
        return $this->materialCode;
    }

    /**
     * Set gSM
     *
     * @param integer $gSM
     *
     * @return SheetMaterial
     */
    public function setGSM($gSM)
    {
        $this->gSM = $gSM;

        return $this;
    }

    /**
     * Get gSM
     *
     * @return int
     */
    public function getGSM()
    {
        return $this->gSM;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     * @return SheetMaterial
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return SheetMaterial
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }
}

