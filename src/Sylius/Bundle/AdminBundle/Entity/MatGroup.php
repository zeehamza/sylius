<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface; 

/**
 * MatGroup
 */
class MatGroup implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $groupCode;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $mRQty;

    /**
     * @var string
     */
    private $markupPercentage;

    /**
     * @var string
     */
    private $stockUnit;

    /**
     * @var string
     */
    private $priceUnit;

    /**
     * @var string
     */
    private $stockConv;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set groupCode
     *
     * @param string $groupCode
     *
     * @return MatGroup
     */
    public function setGroupCode($groupCode)
    {
        $this->groupCode = $groupCode;

        return $this;
    }

    /**
     * Get groupCode
     *
     * @return string
     */
    public function getGroupCode()
    {
        return $this->groupCode;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return MatGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return MatGroup
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set mRQty
     *
     * @param integer $mRQty
     *
     * @return MatGroup
     */
    public function setMRQty($mRQty)
    {
        $this->mRQty = $mRQty;

        return $this;
    }

    /**
     * Get mRQty
     *
     * @return int
     */
    public function getMRQty()
    {
        return $this->mRQty;
    }

    /**
     * Set markupPercentage
     *
     * @param string $markupPercentage
     *
     * @return MatGroup
     */
    public function setMarkupPercentage($markupPercentage)
    {
        $this->markupPercentage = $markupPercentage;

        return $this;
    }

    /**
     * Get markupPercentage
     *
     * @return string
     */
    public function getMarkupPercentage()
    {
        return $this->markupPercentage;
    }

    /**
     * Set stockUnit
     *
     * @param string $stockUnit
     *
     * @return MatGroup
     */
    public function setStockUnit($stockUnit)
    {
        $this->stockUnit = $stockUnit;

        return $this;
    }

    /**
     * Get stockUnit
     *
     * @return string
     */
    public function getStockUnit()
    {
        return $this->stockUnit;
    }

    /**
     * Set priceUnit
     *
     * @param string $priceUnit
     *
     * @return MatGroup
     */
    public function setPriceUnit($priceUnit)
    {
        $this->priceUnit = $priceUnit;

        return $this;
    }

    /**
     * Get priceUnit
     *
     * @return string
     */
    public function getPriceUnit()
    {
        return $this->priceUnit;
    }

    /**
     * Set stockConv
     *
     * @param string $stockConv
     *
     * @return MatGroup
     */
    public function setStockConv($stockConv)
    {
        $this->stockConv = $stockConv;

        return $this;
    }

    /**
     * Get stockConv
     *
     * @return string
     */
    public function getStockConv()
    {
        return $this->stockConv;
    }
}

