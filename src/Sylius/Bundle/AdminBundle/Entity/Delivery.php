<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * Delivery
 */
class Delivery implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $jobNumber;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var time_immutable
     */
    private $time;

    /**
     * @var string
     */
    private $person;

    /**
     * @var string
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return Delivery
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return Delivery
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Delivery
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param time_immutable $time
     *
     * @return Delivery
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return time_immutable
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set person
     *
     * @param string $person
     *
     * @return Delivery
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return string
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Delivery
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}

