<?php

namespace Sylius\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171129115422 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE delivery DROP FOREIGN KEY delivery_ibfk_1');
        $this->addSql('DROP INDEX jobNumber ON delivery');
        $this->addSql('ALTER TABLE delivery CHANGE jobNumber jobNumber VARCHAR(255) NOT NULL');
        $this->addSql('DROP INDEX JobNumber ON job');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Delivery CHANGE jobNumber jobNumber INT NOT NULL');
        $this->addSql('ALTER TABLE Delivery ADD CONSTRAINT delivery_ibfk_1 FOREIGN KEY (jobNumber) REFERENCES job (JobNumber)');
        $this->addSql('CREATE INDEX jobNumber ON Delivery (jobNumber)');
        $this->addSql('CREATE UNIQUE INDEX JobNumber ON Job (JobNumber)');
    }
}
