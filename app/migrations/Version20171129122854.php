<?php

namespace Sylius\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171129122854 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE delivery DROP FOREIGN KEY delivery_ibfk_1');
        $this->addSql('CREATE TABLE ABCDE (id INT AUTO_INCREMENT NOT NULL, one VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE TEST (id INT AUTO_INCREMENT NOT NULL, one VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_EEEA93B87A6C86F1 (one), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE TEST ADD CONSTRAINT FK_EEEA93B87A6C86F1 FOREIGN KEY (one) REFERENCES ABCDE (one)');
        $this->addSql('DROP TABLE delivery');
        $this->addSql('DROP TABLE issue');
        $this->addSql('DROP TABLE job');
        $this->addSql('DROP TABLE jobattributes');
        $this->addSql('DROP TABLE jobmaterial');
        $this->addSql('DROP TABLE jobroute');
        $this->addSql('DROP TABLE material');
        $this->addSql('DROP TABLE matgroup');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE production');
        $this->addSql('DROP TABLE reelmaterial');
        $this->addSql('DROP TABLE sheetmaterial');
        $this->addSql('DROP TABLE supplier');
        $this->addSql('DROP TABLE time');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE TEST DROP FOREIGN KEY FK_EEEA93B87A6C86F1');
        $this->addSql('CREATE TABLE delivery (id INT AUTO_INCREMENT NOT NULL, jobNumber INT NOT NULL, Quantity NUMERIC(10, 0) NOT NULL, Date DATETIME NOT NULL, Time TIME NOT NULL COMMENT \'(DC2Type:time_immutable)\', Person VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, Status VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, INDEX jobNumber (jobNumber), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE issue (id INT AUTO_INCREMENT NOT NULL, JobNumber INT NOT NULL, MaterialCode VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, Quantity NUMERIC(10, 0) NOT NULL, Date DATETIME NOT NULL, Time TIME NOT NULL COMMENT \'(DC2Type:time_immutable)\', PersonCode VARCHAR(30) NOT NULL COLLATE utf8_unicode_ci, Type VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, IssueNumber INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job (id INT AUTO_INCREMENT NOT NULL, JobNumber INT NOT NULL, SONumber INT NOT NULL, DateRequired DATETIME NOT NULL, Product VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, Quantity NUMERIC(10, 0) NOT NULL, Status VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, LinkJobNumber INT NOT NULL, ArtworkAddress VARCHAR(60) NOT NULL COLLATE utf8_unicode_ci, MaterialCost NUMERIC(10, 0) NOT NULL, UNIQUE INDEX JobNumber (JobNumber), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE jobattributes (id INT AUTO_INCREMENT NOT NULL, JobNumber INT NOT NULL, ProductCode VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, AttrName VARCHAR(60) NOT NULL COLLATE utf8_unicode_ci, AttrValue NUMERIC(10, 0) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE jobmaterial (id INT AUTO_INCREMENT NOT NULL, JobNumber INT NOT NULL, MaterialCode VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci, Description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Quantity NUMERIC(10, 0) NOT NULL, Instruction VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE jobroute (id INT AUTO_INCREMENT NOT NULL, JobNumber INT NOT NULL, Sequence INT NOT NULL, MachineCode VARCHAR(20) NOT NULL COLLATE utf8_unicode_ci, Minutes INT NOT NULL, Instruction VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE material (id INT AUTO_INCREMENT NOT NULL, MaterialCode VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci, Description LONGTEXT NOT NULL COLLATE utf8_unicode_ci, MatGroup VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, Type VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, MinimumStock NUMERIC(10, 0) NOT NULL, Status VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, AveragePrice NUMERIC(10, 0) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matgroup (id INT AUTO_INCREMENT NOT NULL, GroupCode VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, Description VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, Type VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, MRQty INT NOT NULL, MarkupPercentage NUMERIC(10, 0) NOT NULL, StockUnit VARCHAR(20) NOT NULL COLLATE utf8_unicode_ci, PriceUnit VARCHAR(20) NOT NULL COLLATE utf8_unicode_ci, StockConv NUMERIC(10, 0) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, PersonCode VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, Name VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, Email VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, Phone VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, Fingerprint SMALLINT NOT NULL, Role VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE production (id INT AUTO_INCREMENT NOT NULL, JobNumber INT NOT NULL, Product VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, Quantity INT NOT NULL, DateProduced DATETIME NOT NULL, TimeProduced TIME NOT NULL COMMENT \'(DC2Type:time_immutable)\', PersonCode VARCHAR(30) NOT NULL COLLATE utf8_unicode_ci, Type VARCHAR(10) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reelmaterial (id INT AUTO_INCREMENT NOT NULL, MaterialCode VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci, GSM INT NOT NULL, Width INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sheetmaterial (id INT AUTO_INCREMENT NOT NULL, MaterialCode VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci, GSM INT NOT NULL, Height INT NOT NULL, Width INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplier (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, description TEXT NOT NULL COLLATE utf8_unicode_ci, enabled TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE time (id INT AUTO_INCREMENT NOT NULL, MachineCode VARCHAR(200) NOT NULL COLLATE utf8_unicode_ci, PersonCode VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, JobNumber INT NOT NULL, Date DATETIME NOT NULL, StartTime TIME NOT NULL COMMENT \'(DC2Type:time_immutable)\', EndTime TIME NOT NULL COMMENT \'(DC2Type:time_immutable)\', QtyProduced INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE delivery ADD CONSTRAINT delivery_ibfk_1 FOREIGN KEY (jobNumber) REFERENCES job (JobNumber)');
        $this->addSql('DROP TABLE ABCDE');
        $this->addSql('DROP TABLE TEST');
    }
}
