<?php declare(strict_types = 1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171214121010 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE delivery DROP INDEX JobNumber, ADD UNIQUE INDEX UNIQ_CEF78E4698A9C92D (JobNumber)');
        $this->addSql('ALTER TABLE delivery CHANGE JobNumber JobNumber INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job MODIFY id INT NOT NULL');
        $this->addSql('DROP INDEX JobNumber ON job');
        $this->addSql('ALTER TABLE job DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE job DROP id');
        $this->addSql('ALTER TABLE job ADD PRIMARY KEY (JobNumber)');
        $this->addSql('ALTER TABLE jobmaterial DROP INDEX JobNumber, ADD UNIQUE INDEX UNIQ_EDF3CEF298A9C92D (JobNumber)');
        $this->addSql('ALTER TABLE jobmaterial DROP INDEX jobmaterial_ibfk_1, ADD UNIQUE INDEX UNIQ_EDF3CEF234474152 (MaterialCode)');
        $this->addSql('ALTER TABLE jobmaterial CHANGE JobNumber JobNumber INT DEFAULT NULL, CHANGE MaterialCode MaterialCode VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE jobmaterial ADD CONSTRAINT FK_EDF3CEF298A9C92D FOREIGN KEY (JobNumber) REFERENCES Job (JobNumber)');
        $this->addSql('DROP INDEX id ON material');
        $this->addSql('ALTER TABLE material DROP id, CHANGE MaterialCode MaterialCode VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Delivery DROP INDEX UNIQ_CEF78E4698A9C92D, ADD INDEX JobNumber (JobNumber)');
        $this->addSql('ALTER TABLE Delivery CHANGE JobNumber JobNumber INT NOT NULL');
        $this->addSql('ALTER TABLE Job DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE Job ADD id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX JobNumber ON Job (JobNumber)');
        $this->addSql('ALTER TABLE Job ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE JobMaterial DROP INDEX UNIQ_EDF3CEF298A9C92D, ADD INDEX JobNumber (JobNumber)');
        $this->addSql('ALTER TABLE JobMaterial DROP INDEX UNIQ_EDF3CEF234474152, ADD INDEX jobmaterial_ibfk_1 (MaterialCode)');
        $this->addSql('ALTER TABLE JobMaterial DROP FOREIGN KEY FK_EDF3CEF298A9C92D');
        $this->addSql('ALTER TABLE JobMaterial CHANGE JobNumber JobNumber INT NOT NULL, CHANGE MaterialCode MaterialCode VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE Material ADD id INT AUTO_INCREMENT NOT NULL, CHANGE MaterialCode MaterialCode VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('CREATE UNIQUE INDEX id ON Material (id)');
    }
}
