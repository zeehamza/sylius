<?php

namespace Sylius\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171130103350 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Receipt (id INT AUTO_INCREMENT NOT NULL, ReceiptNumber INT NOT NULL, PONumber INT NOT NULL, MaterialCode VARCHAR(50) NOT NULL, Quantity NUMERIC(10, 0) NOT NULL, Date DATETIME NOT NULL, Time TIME NOT NULL COMMENT \'(DC2Type:time_immutable)\', Person VARCHAR(30) NOT NULL, Type VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
       // $this->addSql('ALTER TABLE delivery DROP FOREIGN KEY delivery_ibfk_1');
        //$this->addSql('DROP INDEX delivery_ibfk_1 ON delivery');
        //$this->addSql('ALTER TABLE delivery DROP JobNumber');
        //$this->addSql('DROP INDEX JobNumber ON job');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE Receipt');
        $this->addSql('ALTER TABLE Delivery ADD JobNumber INT NOT NULL');
        $this->addSql('ALTER TABLE Delivery ADD CONSTRAINT delivery_ibfk_1 FOREIGN KEY (JobNumber) REFERENCES job (JobNumber)');
        $this->addSql('CREATE INDEX delivery_ibfk_1 ON Delivery (JobNumber)');
        $this->addSql('CREATE UNIQUE INDEX JobNumber ON Job (JobNumber)');
    }
}
