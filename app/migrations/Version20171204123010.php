<?php

namespace Sylius\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171204123010 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE SOLines (id INT AUTO_INCREMENT NOT NULL, SONumber INT NOT NULL, JobNumber INT NOT NULL, ProductCode VARCHAR(50) NOT NULL, QuantityOrdered NUMERIC(10, 0) NOT NULL, PriceUnit NUMERIC(10, 0) NOT NULL, Status VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
       // $this->addSql('ALTER TABLE delivery DROP FOREIGN KEY delivery_ibfk_1');
        //$this->addSql('DROP INDEX delivery_ibfk_1 ON delivery');
        //$this->addSql('ALTER TABLE delivery DROP JobNumber');
        //$this->addSql('DROP INDEX JobNumber ON job');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE SOLines');
        $this->addSql('ALTER TABLE Delivery ADD JobNumber INT NOT NULL');
        $this->addSql('ALTER TABLE Delivery ADD CONSTRAINT delivery_ibfk_1 FOREIGN KEY (JobNumber) REFERENCES job (JobNumber)');
        $this->addSql('CREATE INDEX delivery_ibfk_1 ON Delivery (JobNumber)');
        $this->addSql('CREATE UNIQUE INDEX JobNumber ON Job (JobNumber)');
    }
}
